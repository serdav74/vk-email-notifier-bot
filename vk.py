import logging
from datetime import datetime
from random import randint

import vk_api
from vk_api import ApiError
from vk_api.bot_longpoll import VkBotLongPoll


class Bot:
    def __init__(self, access_token, group):
        self.access_token = access_token
        self.group_id = group

        self.launched = datetime.now()
        self.logger = logging.getLogger('app.bot')

        self.logger.info("Initializing new Bot (group: {}, token: {}...)".format(self.group_id, self.access_token[:3]))

        self.session = vk_api.VkApi(token=self.access_token)
        self.vk = self.session.get_api()
        self.uploader = vk_api.VkUpload(self.vk)
        self.longpoll = VkBotLongPoll(self.session, self.group_id)
        self.logger.info("VK API: success")

    def send_message(self, to, text, attach=None):
        self.logger.debug("Sending message to {}...".format(to))
        try:
            return self.vk.messages.send(
                message=text,
                random_id=randint(1, 1000000),
                peer_id=to,
                attachment=attach or None
            )
        except ApiError:
            self.logger.error("VK API error.", exc_info=1)
            raise

    def event_loop(self):
        for event in self.longpoll.listen():
            print('x')
            print(repr(event.raw))

    def send_long_messages(self, recipients, text, attachments=None, split_by='\n\n'):
        if attachments is None:
            attachments = list()

        messages = []
        message_lines = iter(text.split(split_by))

        while True:
            message_part = ""
            try:
                while len(message_part) < 2500:
                    message_part += next(message_lines) + split_by
            except StopIteration:
                messages.append(message_part)
                break
            messages.append(message_part)

        self.logger.info("Sending messages ({} parts) to {} users".format(len(messages), len(recipients)))
        for chat in recipients:
            for message_piece in messages:
                self.send_message(chat, message_piece, attach=','.join(attachments))

    def post_wall_comment(self, text, post_id=1, owner_id=None):
        owner_id = owner_id or "-" + self.group_id
        self.logger.debug("Posting comment to {}_{}...".format(owner_id, post_id))
        try:
            return self.vk.wall.create_comment(
                owner_id=owner_id,
                post_id=post_id,
                from_id=self.group_id,
                message=text,
                dont_parse_links=1
            )
        except ApiError:
            self.logger.error("VK API error.", exc_info=1)
            raise

    def upload_file(self, filename, title, peer_id):
        attachment = self.uploader.document_message(filename, title, peer_id=peer_id)
        attachment_string = "doc{}_{}".format(attachment['doc']['owner_id'],
                                                 attachment['doc']['id'])
        return attachment_string
