import base64
import json
import logging
import os
import re
import shutil
import sys
from imaplib import IMAP4

import mailparser
from bs4 import BeautifulSoup

from vk import Bot, ApiError


def setup_logging(level='INFO'):
    logger = logging.getLogger('app')
    handler = logging.StreamHandler(stream=sys.stdout)
    formatter = logging.Formatter(fmt="{asctime:>16} | {levelname:8} | {message}", style='{')

    handler.setFormatter(formatter)
    handler.setLevel(level)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger


class App:
    logger = None
    messages = []
    config = None
    bot = None

    def __init__(self):
        with open('config.json') as f:
            config = json.load(f)
        self.config = config
        self.logger = setup_logging(self.config['log_level'])
        self.bot = Bot(access_token=config['vk']['token'], group=config['vk']['group_id'])
        self.logger.info("App initialized.")

    def run(self):
        server, username, password = self.config['server'], self.config['username'], self.config['password']

        with IMAP4(host=server) as imap:
            self.logger.info(f"Starting IMAP4 connection to {server}...")

            imap.starttls()
            imap.login(username, password)
            self.retrieve_inbox(imap)

            for message in self.messages:
                self.send_message(message, self.config['vk']['peers'][0])

            self.mark_as_read(imap)
        self.clean_up()

    def retrieve_inbox(self, connection):
        conn = connection

        self.logger.info("Fetching messages...")

        conn.select()
        typ, data = conn.search(None, 'UNSEEN')  # choose all unseen messages
        for num in data[0].split():
            typ, data = conn.fetch(num, '(RFC822)')
            conn.store(num, '-FLAGS', '\\Seen')  # fetching marks them as Seen; we remove this flag temporarily
            self.messages.append(mailparser.parse_from_bytes(data[0][1]))
        conn.close()

        self.logger.info(f"Found: {len(self.messages)}")

    def mark_as_read(self, connection):
        conn = connection

        self.logger.info('Marking all e-mails as \\Seen...')

        conn.select()
        typ, data = conn.search(None, 'UNSEEN')
        for num in data[0].split():
            conn.store(num, '+FLAGS', '\\Seen')
        conn.close()

    @staticmethod
    def clean_up():
        shutil.rmtree('temp')
        os.mkdir('temp')

    def send_message(self, message, peer_id):
        failed_attaches = []
        attach_ids = []
        for attachment in message.attachments:
            self.logger.info(f"Forwarding {attachment['filename']}...")

            # saving attachment
            local_filename = os.path.join('temp', attachment['filename'])
            with open(local_filename, 'wb') as f:
                f.write(base64.b64decode(attachment["payload"]))

            # uploading attachment
            try:
                attach_id = self.bot.upload_file(local_filename, attachment['filename'], peer_id)
                attach_ids.append(attach_id)
            except ApiError:
                failed_attaches.append(attachment['filename'])
                self.logger.warning(f"File {attachment['filename']} was rejected by VK API.")

        self.bot.send_long_messages(
            recipients=self.config['vk']['peers'],
            # recipients=[self.config['vk']['admin']],
            text=self.format_message(message, failed_attaches),
            attachments=attach_ids[:8],
            split_by='\n')

    @staticmethod
    def format_message(message, failed_attaches):
        text = ""
        if message.text_html:
            soup = BeautifulSoup(message.text_html[0], 'html.parser')
            text = soup.getText(separator='\n').strip()
        elif message.text_plain:
            text = message.text_plain[0].strip()

        text = re.sub('[\\r\\n]+', '\n', text)
        attach_text = ""
        if message.attachments:
            attach_text += f"📎 Вложений: {len(message.attachments)}"
        if failed_attaches:
            attach_text += f" ({len(failed_attaches)} не удалось прикрепить: {', '.join(failed_attaches)})"

        return f"✉ {message.subject}\nОт: {message.from_[0][0]}\n\n{text}\n{attach_text}"


if __name__ == "__main__":
    app = App()
    app.run()
