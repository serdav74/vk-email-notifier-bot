# VK e-mail notifier bot

This script checks for *unread* e-mails in your inbox and sends them to your pm.

## Setting up

1. Clone this repo
1. Create a virtual environment with `python3 -m venv venv`
1. Activate it with `source venv/bin/activate`
1. Install all dependencies with `pip install -r requirements.txt`
1. `deactivate`
1. `cp config.json.example config.json`
1. Fill `config.json` with your values

## Launching
`venv/bin/python app.py`
